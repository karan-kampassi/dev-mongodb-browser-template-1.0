def getFolderName() {
  def array = pwd().split("/")
  return array[array.length - 2];
}
pipeline {
  agent any
  environment {
    BRANCHES = "${env.GIT_BRANCH}"
    COMMIT = "${env.GIT_COMMIT}"
    RELEASE_NAME = "mongodb"
    SERVICE_PORT = "${APP_PORT}"
    DOCKERHOST = "${DOCKERHOST_IP}"
    ACTION = "${ACTION}"
    DEPLOYMENT_TYPE = "${DEPLOYMENT_TYPE == ""? "EC2":DEPLOYMENT_TYPE}"
    KUBE_SECRET = "${KUBE_SECRET}"
    foldername = getFolderName()

  }
  stages {
    stage('init') {
      steps {
        script {

          def job_name = "$env.JOB_NAME"
          print(job_name)
          def values = job_name.split('/')
          namespace_prefix = values[0].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          namespace = "$namespace_prefix-$env.foldername".toLowerCase()
          service = values[2].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          print("kube namespace: $namespace")
          print("service name: $service")
          env.namespace_name=namespace
          env.service=service
        }
      }
    }

    stage('Deploy') {

      steps {
        script {
          echo "echoed folder--- $foldername"

          if (env.DEPLOYMENT_TYPE == 'EC2') {

            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker pull mongo:4.2"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop mongodb || true && docker rm mongodb || true"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker run -d --name mongodb -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=mongoadmin mongo:4.2"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "sleep 5s"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop mongo-express || true && docker rm mongo-express || true"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker run -d --name mongo-express -p $SERVICE_PORT:8081 --link mongodb:mongodb -e ME_CONFIG_MONGODB_SERVER=mongodb -e ME_CONFIG_MONGODB_PORT=27017 -e ME_CONFIG_MONGODB_ENABLE_ADMIN=true -e ME_CONFIG_MONGODB_AUTH_DATABASE=admin -e ME_CONFIG_MONGODB_ADMINUSERNAME=root -e ME_CONFIG_MONGODB_ADMINPASSWORD=mongoadmin mongo-express"'


          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {

            withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''

                    export MONGODB_REPLICA_SET_KEY=$(kubectl get secret --namespace $namespace_name $RELEASE_NAME -o jsonpath="{.data.mongodb-replica-set-key}" | base64 --decode) || true
                    export MONGODB_REPLICA_SET_KEY=${MONGODB_REPLICA_SET_KEY:=""}
                    helm repo add bitnami https://charts.bitnami.com/bitnami || true
                    helm repo add cowboysysop https://cowboysysop.github.io/charts/ || true
                    kubectl create ns "$namespace_name" || true

                    helm upgrade --install $RELEASE_NAME -n "$namespace_name" bitnami/mongodb -f values.yaml --atomic --timeout 300s --set auth.rootPassword="mongoadmin" --set auth.replicaSetKey=$MONGODB_REPLICA_SET_KEY
                    sleep 10
                    export mongodb_server="mongodb-0\\.mongodb-headless\\.$namespace_name\\.svc\\.cluster\\.local\\,mongodb-1\\.mongodb-headless\\.$namespace_name\\.svc\\.cluster\\.local\\,mongodb-2\\.mongodb-headless\\.$namespace_name\\.svc\\.cluster\\.local\\,mongodb-3\\.mongodb-headless\\.$namespace_name\\.svc\\.cluster\\.local"
                    helm upgrade --install $RELEASE_NAME-express -n "$namespace_name" cowboysysop/mongo-express -f express-values.yaml --atomic --timeout 300s --set mongodbServer="$mongodb_server" --set basicAuthUsername="admin" --set basicAuthPassword="MongoPwd"

                    sleep 10
                  '''
                  script {
                    env.temp_service_name = "$RELEASE_NAME-express-mongo-express".take(63)
                    def url = sh (returnStdout: true, script: '''kubectl get svc -n "$namespace_name" | grep "$temp_service_name" | awk '{print $4}' ''').trim()
                    if (url != "<pending>") {
                      print("##\$@\$ http://$url ##\$@\$")
                    }
                  }
            }
          }
        }
      }
    }

    stage('Destroy') {
      when {
        expression {
          env.DEPLOYMENT_TYPE == 'EC2' && env.ACTION == 'DESTROY'
        }
      }
      steps {
        script {
          if (env.DEPLOYMENT_TYPE == 'EC2') {
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop mongodb mongo-express || true && docker rm mongodb mongo-express || true"'
          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {
            withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''
                    helm uninstall $RELEASE_NAME -n "$namespace_name"
                    helm uninstall $RELEASE_NAME-express -n "$namespace_name"
                  '''
            }
          }
        }
      }
    }
  }
}
